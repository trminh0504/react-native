import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import CategoryScreen from '../screens/CategoryScreen';
import HealthScreen from '../screens/HealthScreen';
import HomeScreen from '../screens/HomeScreen';
import QRScreen from '../screens/QRScreen';
import ReportScreen from '../screens/ReportScreen';
const Tab = createBottomTabNavigator();
const CustomTabBarButton =({children, onPress}) =>(
    <TouchableOpacity
    style={{
        top:-30,
        justifyContent: 'center',
        alignItems: 'center',
        ...styles.shadow
    }}
    onPress={onPress}
    >
       <View style={{
           width: 70,
           height: 70,
           borderRadius: 35,
           backgroundColor: '#e32f45'
       }}
       >
           {children}
       </View>
    </TouchableOpacity>
)
const Tabs = () =>{
    return(
        <Tab.Navigator
        tabBarOptions={{
            showLabel:false,
            style:{
                position:'absolute',
                bottom:10,
                left:10,
                right:10,
                elevation:0,
                backgroundColor:'#ffffff',
                borderRadius:15,
                height:70,
                ...styles.shadow 
            }
        }}
        >
            <Tab.Screen name = "Home" component={HomeScreen}
                options = {{
                    tabBarIcon: ({focused}) =>(
                        <View style ={{alignItems: 'center', justifyContent: 'center'}}>
                            <Image
                            source ={require('../assets/icons/home.png')}
                            resizeMode = 'contain'
                            style={{
                                width:25,
                                height:25,
                                tintColor: focused ? '#e32f45' : '#748c94'
                            }}
                            />
                            <Text style={{color: focused ? '#e32f45' : '#748c94' , fontSize:12}}>
                                Home
                            </Text>
                        </View>
                    ),
                }}
            />
            <Tab.Screen name = "Health" component={HealthScreen}
                options = {{
                    tabBarIcon: ({focused}) =>(
                        <View style ={{alignItems: 'center', justifyContent: 'center'}}>
                            <Image
                            source ={require('../assets/icons/health2.png')}
                            resizeMode = 'contain'
                            style={{
                                width:25,
                                height:25,
                                tintColor: focused ? '#e32f45' : '#748c94'
                            }}
                            />
                            <Text style={{color: focused ? '#e32f45' : '#748c94' , fontSize:12}}>
                                Sức khỏe
                            </Text>
                        </View>
                    ),
                }}
            />
            <Tab.Screen name = "QR" component={QRScreen}
                options={{
                    tabBarIcon: ({focused}) =>(
                        <Image
                            source={require('../assets/icons/QR.png')}
                            resizeMode="contain"
                            style={{
                                width:30,
                                height:30,
                                tintColor: '#fff'
                            }}
                        />
                    ),
                    tabBarButton: (props) =>(
                        <CustomTabBarButton{...props} />
                    )
                }}         
            />
            <Tab.Screen name = "Report" component={ReportScreen}
                options = {{
                    tabBarIcon: ({focused}) =>(
                        <View style ={{alignItems: 'center', justifyContent: 'center'}}>
                            <Image
                            source ={require('../assets/icons/report3.png')}
                            resizeMode = 'contain'
                            style={{
                                width:25,
                                height:25,
                                tintColor: focused ? '#e32f45' : '#748c94'
                            }}
                             />
                            <Text style={{color: focused ? '#e32f45' : '#748c94' , fontSize:12}}>
                                Phản ánh
                            </Text>
                        </View>
                    ),
                }}
            />
            <Tab.Screen name = "Category" component={CategoryScreen}
                options = {{
                    tabBarIcon: ({focused}) =>(
                        <View style ={{alignItems: 'center', justifyContent: 'center'}}>
                            <Image
                            source ={require('../assets/icons/category.png')}
                            resizeMode = 'contain'
                            style={{
                                width:25,
                                height:25,
                                tintColor: focused ? '#e32f45' : '#748c94'
                            }}
                             />
                            <Text style={{color: focused ? '#e32f45' : '#748c94' , fontSize:12}}>
                                Danh mục
                            </Text>
                        </View>
                    ),
                }}
            />
        </Tab.Navigator>
    )
}

const styles = StyleSheet.create({
    shadow: {
        shadowColor: '#7F6DF0',
        shadowOffset: {
            width:0,
            height:10,
        },
        shadowOpacity: 0.25,
        shadowRadius:3.5,
        elevation:5,
    }
});

export default Tabs;