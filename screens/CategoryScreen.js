import React from "react";
import { View, Text, Button, StyleSheet} from 'react-native';

const CategoryScreen = ({Navigation}) =>{
    return(
        <View style = {styles.container}>
            <Text 
                style={{
                    textAlign:'center',
                    fontSize: 20,
                }}>
                    Category Screen
            </Text>
        <Button style ={styles.buton}
        title = "Click here"
        onPress = {() => alert('button clicked')}
        />
        </View>
    )
};

export default CategoryScreen;
const  styles = StyleSheet.create({
    container:{
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        backgroundColor: '#8fcbbc'
    },
    buton:{
        width: 40,
        
    }
})