import React from "react";
import { View, Text, Button, StyleSheet} from 'react-native';

const QRScreen = ({Navigation}) =>{
    return(
        <View style = {styles.container}>
            <Text>QR Screen</Text>
        <Button
        title = "Click here"
        onPress = {() => alert('button clicked')}
        />
        </View>
    )
};

export default QRScreen;
const  styles = StyleSheet.create({
    container:{
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        backgroundColor: '#8fcbbc'
    },
})