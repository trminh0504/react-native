import React from "react";
import { View, Text, Button, StyleSheet} from 'react-native';

const ReportScreen = ({Navigation}) =>{
    return(
        <View style = {styles.container}>
            <Text>Report Screen</Text>
        <Button
        title = "Click here"
        onPress = {() => alert('button clicked')}
        />
        </View>
    )
};

export default ReportScreen;
const  styles = StyleSheet.create({
    container:{
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        backgroundColor: '#8fcbbc'
    },
})