import React from "react";
import { View, Text, Button, StyleSheet} from 'react-native';

const HealthScreen = ({Navigation}) =>{
    return(
        <View style = {styles.container}>
            <Text>Health Screen</Text>
        <Button
        title = "Click here"
        onPress = {() => alert('button clicked')}
        />
        </View>
    )
};

export default HealthScreen;
const  styles = StyleSheet.create({
    container:{
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        backgroundColor: '#8fcbbc'
    },
})